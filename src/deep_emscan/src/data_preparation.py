import os
import numpy as np
import pandas as pd

gene_data_df = pd.read_csv('../data/gene_data.csv')
gene_data_df['chromosome'] = 'chr' + gene_data_df['chromosome']

motif1_path = '../data/pwm_tfbs_per_tf/GATA1/'
motif2_path = '../data/pwm_tfbs_per_tf/TAL1/'
motif1 = motif1_path.split('/')[-2]
motif2 = motif2_path.split('/')[-2]
# print(motif1, motif2)

data_save_path = '../data/'+ motif1 + '_' + motif2 + '/'
if not os.path.isdir(data_save_path):
    os.mkdir(data_save_path)

chromosomes_considered = ['chr1', 'chr2', 'chr3', 'chr4', 'chr5', 'chr6', 'chr7', 'chrX',
       'chr8', 'chr9', 'chr11', 'chr10', 'chr12', 'chr13', 'chr14',
       'chr15', 'chr16', 'chr17', 'chr18', 'chr20', 'chr19', 'chrY',
       'chr22', 'chr21']

def join_binding_sites(motif_filepath):
    col_names = ['chromosome', 'start_pos', 'end_pos', 'bs_seq', 'score', 'strand']
    motif_df = pd.DataFrame(columns=col_names)

    for rootpath, dirpath, filenames  in os.walk(motif_filepath):
        for filename in filenames:
            filepath = rootpath + filename
            temp_df = pd.read_csv(filepath, sep='\t',names = col_names)
            motif_df = pd.concat([motif_df, temp_df], ignore_index=True)

    return motif_df

motif1_df = join_binding_sites(motif1_path)
# print(motif1_df.shape)

motif2_df = join_binding_sites(motif2_path)
# print(motif2_df.shape)

motif1_df = motif1_df[motif1_df['chromosome'].isin(chromosomes_considered)]
motif2_df = motif2_df[motif2_df['chromosome'].isin(chromosomes_considered)]
gene_data_df = gene_data_df[gene_data_df['chromosome'].isin(chromosomes_considered)]

# print(motif1_df.shape)
# print(motif2_df.shape)

motif1_df.drop_duplicates(subset=['chromosome', 'bs_seq', 'start_pos', 'strand'], inplace = True)
motif1_df = motif1_df.reset_index(drop = True)
motif1_df['index'] = motif1_df.index
motif1_df.to_csv(data_save_path + '/' + motif1 + '_sequences.csv', index = False)

motif2_df.drop_duplicates(subset=['chromosome', 'bs_seq', 'start_pos', 'strand'], inplace = True)
motif2_df = motif2_df.reset_index(drop = True)
motif2_df['index'] = motif2_df.index
motif2_df.to_csv(data_save_path + '/' + motif2 + '_sequences.csv', index = False)

df_cols = [motif1 + '_ix', motif2 + '_ix', 'dbbs']
_1k_df = pd.DataFrame(columns = df_cols)
_1k_5k_df = pd.DataFrame(columns = df_cols)
_5k_10k_df = pd.DataFrame(columns = df_cols)
_summary_df = pd.DataFrame(columns=[motif1 + '_ix', '_1k', '1k_5k', '5k_10k', '10k_'])

# chromosome = 'chr1'
for chromosome in chromosomes_considered:
    temp1_df = motif1_df[motif1_df['chromosome'] == chromosome]

    for ix, row in temp1_df.iterrows():
        bs1_index = row['index']
        temp2_df = motif2_df[motif2_df['chromosome'] == chromosome]
        temp2_df = temp2_df[temp2_df['strand'] == row['strand']]
        temp2_df[motif1 + '_ix'] = [bs1_index] * len(temp2_df)
        temp2_df['dbbs'] = abs(int(row['start_pos']) - temp2_df['start_pos'].astype(int))
        temp2_df.rename(columns={'index': motif2 + '_ix'}, inplace=True)
        temp2_df = temp2_df[df_cols]

        _1k_temp_df = temp2_df[temp2_df['dbbs'] <= 1000]
        _1k_df = pd.concat([_1k_df,_1k_temp_df], axis=0).reset_index(drop=True)

        _1k_5k_temp_df = temp2_df[(temp2_df['dbbs'] > 1000) & (temp2_df['dbbs'] <= 5000)]
        _1k_5k_df = pd.concat([_1k_5k_df,_1k_5k_temp_df], axis=0).reset_index(drop=True)

        _5k_10k_temp_df = temp2_df[(temp2_df['dbbs'] > 5000) & (temp2_df['dbbs'] <= 10000)]
        _5k_10k_df = pd.concat([_5k_10k_df,_5k_10k_temp_df], axis=0).reset_index(drop=True)

        _summary_temp_df = pd.DataFrame(columns=[motif1 + '_ix', '_1k', '1k_5k', '5k_10k', '10k_'])
        _summary_temp_df.loc[0] = [bs1_index, len(_1k_temp_df), len(_1k_5k_temp_df), len(_5k_10k_temp_df),
                                   len(temp2_df[temp2_df['dbbs'] > 10000])]
        _summary_df = pd.concat([_summary_df,_summary_temp_df], axis=0).reset_index(drop=True)

    _1k_df.to_csv(data_save_path + '_1k_co_occurrence.csv', index = False)
    _1k_5k_df.to_csv(data_save_path + '1k_5k_co_occurrence.csv', index = False)
    _5k_10k_df.to_csv(data_save_path + '5k_10k_co_occurrence.csv', index = False)
    _summary_df.to_csv(data_save_path + 'summary_co_occurrence.csv', index = False)
