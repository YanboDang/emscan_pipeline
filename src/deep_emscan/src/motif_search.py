import pandas as pd
import os

def join_binding_sites(motif_filepath):
    col_names = ['chromosome', 'start_pos', 'end_pos', 'bs_seq', 'score', 'strand']
    motif_df = pd.DataFrame(columns=col_names)

    for rootpath, dirpath, filenames  in os.walk(motif_filepath):
        for filename in filenames:
            filepath = rootpath + filename
            temp_df = pd.read_csv(filepath, sep='\t',names = col_names)
            motif_df = pd.concat([motif_df, temp_df], ignore_index=True)

    return motif_df

gene_data_df = pd.read_csv('../data/gene_data.csv')
gene_data_df['chromosome'] = 'chr' + gene_data_df['chromosome']

data_df = pd.read_csv('../data/GATA1_TAL1/1k_data_features.csv')
temp1 = data_df[(data_df['dbbs'] <= 50) & (data_df['dng'] <= 1500)]
# Unique gene occurrences
temp1 = temp1[['ng', 'chromosome', 'strand']]
temp1 = temp1.drop_duplicates().reset_index(drop = True)

all_motifs_filepath = '../data/pwm_tfbs_per_tf/'
all_motif_paths = []
for rootpath, dirpath, filenames  in os.walk(all_motifs_filepath):
    for folder in dirpath:
        all_motif_paths.append(rootpath + folder + '/')

all_motif_dfs = {}
all_motifs = []
for ix, motif_path in enumerate(all_motif_paths):
    motif_df = join_binding_sites(motif_path)
    motif_df.drop_duplicates(subset=['chromosome', 'bs_seq', 'start_pos', 'strand'], inplace = True)
    motif_df['start_pos'] = motif_df['start_pos'].astype(int)
    motif_df = motif_df.reset_index(drop = True)
    motif = motif_path.split('/')[3]
    all_motifs.append(motif)
    all_motif_dfs[motif] = motif_df
    # print('Percentage completed: ', round(ix/len(all_motif_paths) * 100, 0), end = '\r')


start_positions = []
nearest_motifs = []

for ix,row in temp1.iterrows():
    nearest_motif = {}
    gene = row['ng']
    chromosome = row['chromosome']
    strand = row['strand']
    start_pos = gene_data_df[(gene_data_df['gene_name'] == gene) &
                            (gene_data_df['chromosome'] == chromosome) &
                            (gene_data_df['strand'] == strand)]['position_start']

    for position in start_pos:
        for motif in all_motifs:
            motif_df = all_motif_dfs[motif]
            motif_df = motif_df[(motif_df['chromosome'] == chromosome) &
                                (motif_df['strand'] == strand)]
            motif_df['dng'] = int(position) - motif_df['start_pos']
            motif_df = motif_df[(motif_df['dng'] >= 0) &
                                (motif_df['dng'] <= 1500)]

            if motif_df.shape[0]:
                for jx, row2 in motif_df.iterrows():
                    nearest_motif[motif] = row2['dng']

    start_positions.append(position)
    nearest_motifs.append(nearest_motif)

    # print(gene, 'Percentage completed: ', round(ix/len(temp1) * 100, 0), end = '\r')

temp1['gene_start_pos'] = start_positions
temp1['nearest_motifs'] = nearest_motifs

temp1.to_csv('../data/GATA1_TAL1/motif_search.csv', index = False)
