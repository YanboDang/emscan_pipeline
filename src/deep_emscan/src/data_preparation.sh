#!/bin/bash
#SBATCH --job-name="Data preparation GATA_TAL all chr"
#SBATCH --time=20:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --output=myjob.%j.o
#SBATCH --error=myjob.%j.e
#SBATCH --account=lz25
#SBATCH --export=NONE
#======START=====

module load python/3.7.3-system
python3 /home/tbon0008/lz25_scratch/tbon/deep_emscan/src/data_preparation.py
