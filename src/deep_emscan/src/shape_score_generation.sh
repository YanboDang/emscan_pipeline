#!/bin/bash
#SBATCH --job-name="Shift score generation"
#SBATCH --time=100:00:00
#SBATCH --nodes=2
#SBATCH --ntasks=2
#SBATCH --cpus-per-task=2
#SBATCH --output=myjob.%j.o
#SBATCH --error=myjob.%j.e
#SBATCH --account=lz25
#SBATCH --export=NONE
#======START=====

module load python/3.7.3-system
python3 /home/tbon0008/lz25_scratch/tbon/deep_emscan/src/shape_score_generation.py
