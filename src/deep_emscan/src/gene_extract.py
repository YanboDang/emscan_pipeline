import re
import pandas as pd

# filename = '/Users/tarunbonu/Tarun/Sem_3/minor_thesis/emscan_pipeline/data/Homo_sapiens_sample_100.gtf'
# filename = '/home/tbon0008/lz25_scratch/tbon/emscan_pipeline/data/Homo_sapiens_sample_100.gtf'
filename = '/home/tbon0008/lz25_scratch/Homo_sapiens.GRCh38.87.gtf'

with open(filename, 'r') as fp:
    gene_data_df = pd.DataFrame(
        columns=['chromosome','gene_name', 'gene_id', 'position_start', 'position_end', 'gene_length', 'gene_source', 'strand'])
    df_ix = 0
    
    line = fp.readline()
    cnt = 1
    
    while line:
        line_elements = line.split('\t')
        if len(line_elements) > 1 and line_elements[2] == 'gene':
            
            gene_id = re.search(r'gene_id "(.*?)"', line_elements[8]).group(1)
            gene_name = re.search(r'gene_name "(.*?)"', line_elements[8]).group(1)
            gene_source = re.search(r'gene_source "(.*?)"', line_elements[8]).group(1)            
        
            gene_data_df.loc[df_ix] = [line_elements[0], gene_name, gene_id, line_elements[3], line_elements[4], int(line_elements[4])-int(line_elements[3]),gene_source, line_elements[6]]
            df_ix += 1
            
        line = fp.readline()
        cnt += 1
        
gene_data_df.to_csv('../data/gene_data.csv', index = False)
