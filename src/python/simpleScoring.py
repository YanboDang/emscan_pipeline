import sys
import optparse
from cal_scores import convert_matrix
from evoScore import get_negative_strand


def pattern_search(seqs, motif_matrix):
    seqs = seqs.split('\n')[:-1]
    window_size = len(motif_matrix) - 2

    # Sequences of all the species
    for ix in range(0, len(seqs)-1, 2):
        species = seqs[ix]
        positive_base_seq = seqs[ix + 1]
        # print(species)
        print(positive_base_seq)

        # species, positive_base_seq = seq.split(' \t ')
        positive_base_seq = positive_base_seq.lower()
        negative_base_seq = get_negative_strand(positive_base_seq)
        # print('------------------')
        # print(positive_base_seq, negative_base_seq)
        scores = []

        for start_pos in range(0, len(positive_base_seq) - window_size):
            end_pos = start_pos + window_size + 1
            pos_sub_seq = positive_base_seq[start_pos: end_pos]
            neg_sub_seq = negative_base_seq[start_pos: end_pos]

            if ('-' in pos_sub_seq) and ('-' not in neg_sub_seq):
                neg_score = cal_score(neg_sub_seq, motif_matrix)
                pos_score = 0
            if ('-' in neg_sub_seq) and ('-' not in pos_sub_seq):
                pos_score = cal_score(pos_sub_seq, motif_matrix)
                neg_score = 0
            if ('-' not in pos_sub_seq) and ('-' not in neg_sub_seq):
                pos_score = cal_score(pos_sub_seq, motif_matrix)
                neg_score = cal_score(neg_sub_seq, motif_matrix)
            else:
                continue

            if pos_score > neg_score:
                scores.append(('+ve', pos_sub_seq, pos_score, start_pos))
                print('+ve', pos_sub_seq, pos_score, start_pos)
            else:
                scores.append(('-ve', neg_sub_seq, neg_score, start_pos))
                print('-ve', neg_sub_seq, neg_score, start_pos)

        # print(species, scores)


# attatatTATTAATTATTTGAATTCTTGTTTCTTGTAATGCTTGATTACT
def cal_score(sub_seq, motif):
    score = 0.0
    for ix, base in enumerate(sub_seq):
        if base == 'a':
            score += float(motif[ix + 1][0])
        elif base == 'c':
            score += float(motif[ix + 1][1])
        elif base == 'g':
            score += float(motif[ix + 1][2])
        elif base == 't':
            score += float(motif[ix + 1][3])

    return score


# pattern_search(seqs, motif_matrix)

if __name__ == "__main__":
    parser = optparse.OptionParser(
        usage="python simpleScoring.py -p -s")
    parser.add_option('-p', '--pscm', action='store',
                      dest='matrix', help='The filepath for the motif positional matrix')
    parser.add_option('-f', '--format', action='store',
                      dest='tformat', help='The format of the motif positional matrix, \'transfac\' or \'jaspar\'')
    parser.add_option('-s', '--seq', action='store',
                      dest='seq', help='The filepath for the species subsequence')


    (args, _) = parser.parse_args()
    if args.matrix == None or args.seq == None:
        print("Missing required arguments")
        sys.exit(1)

    with open(args.matrix, 'r') as f_open:
        motif_matrix = f_open.read()

    with open(args.seq, 'r') as f_open:
        seqs = f_open.read()


    motif_matrix = convert_matrix(motif_matrix, args.tformat)

    # print(seqs)
    # print(motif_matrix)

    pattern_search(seqs, motif_matrix)


    #
    # app.run(host='0.0.0.0', port=int(args.port),
    #         debug=True)
