# Import necessary libraries
import math
from substitution import prob_b_given_a_JK #SONIKA changed `S` to `s` in the package name
from substitution import prob_b_given_a_fixation
from substitution import prob_b_given_a_HKY

def get_negative_strand(positive_strand):
    """
    Generates a reverse-complimentary sequence of the given sequence
    """
    negative_strand = ""
    for base in positive_strand[::-1]: # Reversing the input sequence

        # Complementing them based on the base values
        if base  == 'a':
            negative_strand += 't'
        elif base == 't':
            negative_strand += 'a'
        elif base == 'c':
            negative_strand += 'g'
        elif base == 'g':
            negative_strand += 'c'
        elif base == '-':
            negative_strand += '-'
        else: # Unexpected character in sequencee
            print('Unexpecteed character in sequence while generating negative strand')
            return 1
    return negative_strand


def calculate_window_score(window_seqs, window_size, motif_matrix, scale_lambda, branch_length, mu, K, model):
    """
    Calculates the window score using respective formulation based on the given model
    Returns window score
    """
    window_score = 0
    base_pos = {'a': 0, 'c': 1, 'g': 2, 't':3} # Letter mappings to access the PWM values from the motif

    for pos in range(window_size): # Going through all the positions in iteration
        # Initialisations for HKY
        pos_score = 1

        all_letters = {window_seq[pos] for window_seq in window_seqs} # Extracting all the distinct base occurrences
        for base1 in all_letters:
            for base2 in all_letters:
                if model == 'jc':
                    pos_score *= prob_b_given_a_JK(base1, base2, scale_lambda, branch_length)
                elif model == 'fix':
                    f_b = float(motif_matrix[pos + 1][base_pos[base2]])
                    pos_score *= prob_b_given_a_fixation(base1, base2, f_b, mu)
                if model == 'hky':
                    pos_score *= prob_b_given_a_HKY(base1, base2, motif_matrix, pos, branch_length, K)

        window_score += pos_score # Append the poitional score to the window score
    return window_score



def get_evoscore(pos_seqs_dict, motif_matrix, scale_lambda, branch_length, p, q, model):
    """
    Generate evolutionary scores for all the windows in the sequence by sliding a window through
    the whole of the sequence

    Prints the position and the scores associated with windows at those positions
    """

    window_size = len(motif_matrix) - 1 # Calculating the window size
    # shift_by = 1
    # Fixation mu
    mu = scale_lambda * 1 # Calculate mu from lambda

    # HKY K
    K = - math.log((1 - 2*p - q) * math.sqrt(1 - 2*q)) / 2 # Calculate K from p,q

    seqs = list(pos_seqs_dict.values()) # Get all the sequences from the dictionary
    min_seq_len = min([len(seq) for seq in seqs]) # Sequences might be of different lengths. Iterate till the minimum value to keep the window consistent

    for k in range(min_seq_len - window_size + 1): # Slide through the sequences with a window and calculate window scores
        # print('------------')
        pos_window_seqs = [seq[k:k+window_size] for species,seq in pos_seqs_dict.items()] # Generate window sequences of all species
        neg_window_seqs = [get_negative_strand(pos_seq) for pos_seq in pos_window_seqs] # Generate the negative strand window sequences

        pos_window_score = calculate_window_score(pos_window_seqs, window_size, motif_matrix, scale_lambda, branch_length, mu, K, model) # Calculate positive window score
        neg_window_score = calculate_window_score(neg_window_seqs, window_size, motif_matrix, scale_lambda, branch_length, mu, K, model) # Calculate negative window score


        window_score = max(pos_window_score, neg_window_score) # Window score is the maximum of the positive and the negative windows
        print(k, window_score)
