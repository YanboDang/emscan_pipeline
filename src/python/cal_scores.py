# Importing required libraries
import math
import re
import optparse
import sys
from evoScore import get_evoscore

def convert_matrix(matrix, format):
    """
    Read a PWM file with the positional weight matrix (PWM) into positional list
    for easier access of base values based on positions
    """

    # Read and parse the TRANSFAC file format
    if format == 'transfac':
        matrix = re.search(r'(PO[\w\W]*?)XX', matrix)[1] # Capture only the Matrix values
        positions = matrix.split('\n')[:-1] # Split into lines
        positions = [position.split('\t')[1:] for position in positions] # Generate a list of lists
        return positions

    # Read and parse the JASPAR file format
    elif format == 'jaspar':
        matrix = matrix.split('\n')[1:-1] # Capture only the Matrix values
        matrix = [line.split() for line in matrix]
        positions = [[None] * 4 for i in range(len(matrix[0]) - 2)] # Creating an empty Positional list
        # Transpose the matrix values to fit into required format
        for ix,line in enumerate(matrix):
            jx = 0
            for score in line:
                if score not in ['[', ']']: # Ignore irrelevant brackets
                    positions[jx][ix] = score # Update scores onto the positions
                    jx += 1
        return positions

def add_pseudo_count(matrix, pseudo_count):
    """
    Add given pseudocount value to all the
    values of the matrix
    Returns a matrix with non-zero frequency values
    """
    for ix, row in enumerate(matrix[1:]):
        for jx, value in enumerate(row):
            if value == '0.0':
                matrix[ix + 1][jx] = float(pseudo_count)
            else:
                matrix[ix + 1][jx] = float(value) + pseudo_count

    return matrix

def pfm_to_pwm(pfm):
    """
    Converts PFM to PWM
    Returns a PWM
    """
    pwm = []
    pwm.append(pfm[0])

    for row in pfm[1:]:
        row_sum = sum(row)
        new_row = [value/row_sum for value in row]
        pwm.append(new_row)

    return pwm


def get_formatted_seqs(seqs):
    """
    Format the input of multiple sequences into a standard sequence to
    pass through the EMSCAN pipeline

    Returns a dictionary of Species:Sequence pairs
    """
    seqs = seqs.split('\n')

    all_seqs = {} # Creating an empty dictionary
    ix = 0
    inloop = 0

    # Read through the sequence line by line to generate one sequence for one species
    while(ix < len(seqs)):
        line = seqs[ix]
        seq = ""
        if line.startswith('>'):
            species = line[1:]
            ix += 2 # Skip the line which has offset values
            line = seqs[ix]
            # Capture the sequence till the start of next sequence is detected
            while((not line.startswith('>')) and (ix+1 < len(seqs))):
                seq += line.lower()
                ix += 1
                line = seqs[ix]
                inloop = 1

        # Assign it to the dictionary
        if inloop:
            all_seqs[species] = seq
            ix -= 2 # Reduce the index to again capture the required sequence
        ix += 1
        inloop = 0

    return all_seqs


if __name__ == "__main__":
    """
    The main function to be called with options to get the pipeline working.
    This is the entry point to the EMSCAN pipeline
    """

    # The usage of the script with options specified
    parser = optparse.OptionParser(
        usage="python cal_scores.py -t -tf -s -m")

    # Parsing through the optional inputs provided on the command line
    parser.add_option('-p', '--pscm', action='store',
                      dest='pscm', help='The filepath for the motif positional matrix')
    parser.add_option('-f', '--format', action='store',
                      dest='format', help='The format of the motif positional matrix, \'transfac\' or \'jaspar\'')
    parser.add_option('-s', '--sfile', action='store',
                      dest='seqs', help='The filepath for the species subsequence')
    parser.add_option('-m', '--mod', action='store',
                      dest='model', help='The modeltype, \'jc\' or \'fix\' or \'hky\'')

    (args, _) = parser.parse_args() # Extract all the arguments from the parsed options
    # Stop if any required options are not specified
    if args.pscm == None or args.seqs == None or args.model == None or args.format == None:
        print("Missing required arguments")
        sys.exit(1)

    # Open the file to get the PWM text
    with open(args.pscm, 'r') as f_open:
        motif_matrix = f_open.read()

    motif_matrix = convert_matrix(motif_matrix, args.format) # Prase the text and convert into PFM format
    pseudo_count = 1 # Default value is set to 1
    motif_matrix = add_pseudo_count(motif_matrix, pseudo_count) # Add the pseudo count to the PFM
    motif_matrix = pfm_to_pwm(motif_matrix) # Convert the PFM to PWM as a standard input

    # Open the sequence file and read the content
    with open(args.seqs, 'r') as f_open:
        seqs = f_open.read()
        seqs = get_formatted_seqs(seqs) # Pre-process the sequences into standard input format

    # print(seqs)
    # print(neg_seqs)
    # print(motif_matrix)
    # print(args.model)

    # Call relevant functions based on the desired scoring techniques
    scale_lambda = 0.1
    branch_length = 0.90091
    hky_p = 0.07
    hky_q = 0.03
    model = args.model

    get_evoscore(seqs, motif_matrix, scale_lambda, branch_length, hky_p, hky_q, model)
