import math
import sys
import optparse
from simpleScoring import generate_negative_strand, convert_matrix

def cal_JC_score(seq, motif_seq, branch_length, scale_lambda):
    prob_score = 0.0

    for ix, motif_base in enumerate(motif_seq):
        if motif_base == seq[ix]:
            prob_score += ((1/4) + ((3/4) * math.exp((4/3) * -(scale_lambda * branch_length))))
        else:
            ((1/4) - ((1/4) * math.exp((4/3) * -(scale_lambda * branch_length))))

    return prob_score

def cal_Fixation_score(seq, motif_seq, motif, scale_lambda):

    prob_score = 0.0
    mu = scale_lambda * 1 # Need to know the derivation of mu with lambda
    base_pos = {'a': 0, 'c': 1, 'g': 2, 't':3}

    for ix, motif_base in enumerate(motif_seq):

        if motif_base == seq[ix]:
            prob_score += (mu + float(motif[ix + 1][base_pos[motif_base]]))
        else:
            prob_score += (mu + float(motif[ix + 1][base_pos[motif_base]]) + (1 - mu))

    return prob_score

def pattern_search(seqs, motif_seq, motif_matrix, model):
    seqs = seqs.split('\n')[:-1]
    window_size = len(motif_seq) - 1
    scale_lambda = 12
    branch_length = 1
    motif_matrix = convert_matrix(motif_matrix)[:-1]

    # Sequences of all the species
    for seq in seqs:
        species, positive_base_seq = seq.split('   ')
        positive_base_seq = positive_base_seq.lower()
        negative_base_seq = generate_negative_strand(positive_base_seq)
        scores = []

        for start_pos in range(0, len(positive_base_seq) - window_size):
            end_pos = start_pos + window_size + 1
            pos_sub_seq = positive_base_seq[start_pos: end_pos]
            neg_sub_seq = negative_base_seq[start_pos: end_pos]

            if model:

                if ('-' in pos_sub_seq) and ('-' not in neg_sub_seq):
                    neg_score = cal_JC_score(neg_sub_seq, motif_seq, branch_length, scale_lambda)
                    pos_score = 0
                if ('-' in neg_sub_seq) and ('-' not in pos_sub_seq):
                    pos_score = cal_JC_score(pos_sub_seq, motif_seq, branch_length, scale_lambda)
                    neg_score = 0
                if ('-' not in pos_sub_seq) and ('-' not in neg_sub_seq):
                    pos_score = cal_JC_score(pos_sub_seq, motif_seq, branch_length, scale_lambda)
                    neg_score = cal_JC_score(neg_sub_seq, motif_seq, branch_length, scale_lambda)
                else:
                    break
            else:
                if ('-' in pos_sub_seq) and ('-' not in neg_sub_seq):
                    neg_score = cal_Fixation_score(neg_sub_seq, motif_seq, motif_matrix, scale_lambda)
                    pos_score = 0
                if ('-' in neg_sub_seq) and ('-' not in pos_sub_seq):
                    pos_score = cal_Fixation_score(pos_sub_seq, motif_seq, motif_matrix, scale_lambda)
                    neg_score = 0
                if ('-' not in pos_sub_seq) and ('-' not in neg_sub_seq):
                    pos_score = cal_Fixation_score(pos_sub_seq, motif_seq, motif_matrix, scale_lambda)
                    neg_score = cal_Fixation_score(neg_sub_seq, motif_seq, motif_matrix, scale_lambda)
                else:
                    break

            if pos_score > neg_score:
                scores.append(('+ve', pos_sub_seq, pos_score, start_pos))
            else:
                scores.append(('-ve', neg_sub_seq, neg_score, start_pos))

        print(species, scores)


# pattern_search(seqs, motif_seq, motif_matrix)

if __name__ == "__main__":
    parser = optparse.OptionParser(
        usage="python evoScore.py -p -s")
    parser.add_option('-p', '--pwm', action='store',
                      dest='matrix', help='The filepath for the motif positional matrix')
    parser.add_option('-s', '--seq', action='store',
                      dest='seq', help='The filepath for the species subsequence')
    parser.add_option('-m', '--mod', action='store',
                      dest='model', help='The modeltype, \'jc\' or \'fix\'')

    (args, _) = parser.parse_args()
    if args.matrix == None or args.seq == None:
        print("Missing required arguments")
        sys.exit(1)

    with open(args.matrix, 'r') as f_open:
        motif_matrix = f_open.read()

    with open(args.seq, 'r') as f_open:
        seqs = f_open.read()

    print(seqs)
    print(motif_matrix)
    print(args.model)
    motif_seq = "attatattat"
    if args.model == 'jc':
        pattern_search(seqs, motif_seq, motif_matrix, 1)
    elif args.model == 'fix':
        pattern_search(seqs, motif_seq, motif_matrix, 0)
