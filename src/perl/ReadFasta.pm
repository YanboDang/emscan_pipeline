package ReadFasta;

=head2 new

 Title   : new
 Usage   : my $get_fasta = new(
			       '-sfile' => "filename"
			       );
           
 Function: Get the block/sequence data from CLUSTAL W
           multiple alignment output.
 Returns : reference to an array
 Args    : filename

=cut

sub new{

  my $class = shift;
  my %args = @_;
  my $self = bless {}, ref($class) || $class;

  if(defined $args{-sfile}) {
    open(FILE, "$args{-sfile}") || die "$!\n";

    my @data = <FILE>;			# read in the entire file

    if ($data[0] =~ /##GAPLESS_BLOCK_FORMAT##/) {
      $self->_gapless_fasta($args{-sfile});
      @data=();
    } else {
      print "Input file format is not a valid GAPLESS format:\n";
      exit(1);
    }
  } else {
    print stderr "No input sequences !! \n"; exit(0);
  }

  return $self;
}

=head2
Title   : gapless_fasta 
Function: reads in and stores gapless block data 
Returns : obejects of type ReadFasta class. 
Args    : Filename

=cut

  sub _gapless_fasta{

  $self=shift; 

  (@seqid,@sequence,@spos,@block,@BLOCK)=();

  my $flag =0;

  $file=shift;
  open(FH,$file) || die "$!";
  $i=0;
  my $flag2=0;

  while(<FH>) {
    if(/^##GAPLESS_BLOCK_FORMAT#/) {
    #print "File data\n"; 
	    if($flag2) { 
		    @BLOCK=([@block_pos],[@block_seq]);
		    $flag2=0;
		    (@block_pos,@block_seq)=();
	    }	
    }	elsif(/^#GAPLESS_BLOCK/) { 
      if($flag) {
        $i++;
        #print "Read $i blocks @seqid\n";
	push(@block_pos,[@spos]);
	push(@block_seq,[@sequence]);
	$flag=0;
	$flag2=1;
	@seqid = @seqid1;
	(@getp,@sequence,@spos)=();
      }
    } elsif(/>/) {
      #split(); 
      s/>//;
      s/\n$//;
      push(@seqid1,$_);
      $flag=1; 
    } elsif(/^#SEQ_Start_offset/) {
      @getp=split/\s+/;	
      push(@spos,$getp[1]);
    } elsif($_ !~ /#/ and $_ !~ /^$/) {
     s/\n$//;
     push(@sequence,$_);
    }
  }
  @seqid = @seqid1;
  push(@block_pos,[@spos]);
  push(@block_seq,[@sequence]);
  (@getp,@sequence,@spos)=();
  @BLOCK=([@block_pos],[@block_seq]);
  $flag2=0;
  (@block_pos,@block_seq)=();
  $self->{seq_id}=\@seqid;

  return $self->{gapless_data}=[@BLOCK];

}





sub DESTROY  {
    # nothing
}
1;
