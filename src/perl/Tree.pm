=head1 NAME Tree.pm
This package implements directed trees.

Nodes contain:
	name		string, (required if leaf)
	clist		list of children (array of node ptrs)
	t		distance to parent (unless no parent)
	mu1		setable attribute
	mu0		setable attribute
	row		setable attribute
=cut

#code begins here
package Tree;

=head2
#
# new
#
#  Create a tree node.
#  Input arguments are a hash containing (some or all of):
#	'-name' => name, Name of the node,
#	'-clist' => clist, pointer to the array of its children list,
#	'-t'	=> t, distance to parent
=cut
sub new {

 $class = shift;
 %args = @_;
 $self = bless {}, $class;

 if(defined $args{-name}) {
   $self->{name} = $args{-name};
 }
 if(defined $args{-clist}) {
   $self->{clist} = $args{-clist};
 }
 if(defined $args{-t}) {
   $self->{t} = $args{-t};
 }

  return $self;
} # new

#
# read_newick
#
#  Read Newick file and return a pointer to a node (tree).
#  Newick format is documented in:
#  (http://evolution.genetics. washington.edu/phylip/newicktree.html).
#  e.g. (B:6.0,(A:5.0,C:3.0,E:4.0):5.0,D:11.0), where
#  letters represent the leaves of a tree. The two matched set of parenteses
#  represent the internal nodes. Numbers after the colons are the branch length
#  from the node. The internal node is represented by the enclosing parentheses.
#
sub read_newick{
  my(
   $self, 				# package name
   $file				# file containing tree in Newick format
  ) = @_;

  open(FH, $file) || die "Cannot open file '$file': $!.\n";
  my (@lines) = <FH>;
  close(FH);
  $input_string = join "", @lines;
  $input_string =~ s/\s+//g;
  my ($node_obj,$length) = parse_newick($input_string, 0, 0);
  return $node_obj;
}


=head2
Title   : parse_newick
Function: parses the tree given in Newick format,
          (as shown above), creates node objects.
Returns : Node object
Args    : string to be parse and index pointing
          to "(", the left parantheses.
=cut
sub parse_newick {
  my(
     $string,         # string containing a tree in Newick format
     $index,	      # Position in the string where the tree starts
     $n		      # number of unnamed internal nodes so far
   ) = @_;

  #parse string from the index; (index must point to '(')
  # until ")" found
  # return ptr. to the node (tree) and the length of the
  # string consumed

  my $word = "";
  my @clist = ();
  my $t;
  my $c;
  my $i;
  my $need_comma = 0;
  $index++;
  my $token_start = $index;
  for($i=$index; $i<length($string); $i++) {
     $c = substr($string, $i,1);
     if($c eq ":") {       # end of node-name
       if ($need_comma) {
	 print "missing comma prior to here\n", substr($string,0,$i+1), "\n";
	 exit();
       }
       if (! defined $subtree) {
         $len = $i - $token_start;
         if($len <=0) {
           print "missing 'Name' prior to here\n", substr($string,0,$i+1), "\n";
           exit();
         }
         $name = substr($string,$token_start,$len);
         $subtree = Tree->new('-name' => $name, '-t' => $t); # Make leaf
       }
       $need_comma = 1;
       $token_start = $i +1;
     } elsif($c eq ',' || $c eq ')') {            # End of the distance from the parent node
       # Get distance and check that it is OK
       $len = $i - $token_start;
       if($len <=0) {
	 print "missing 'Distance'  prior to here\n", substr($string,0,$i+1), "\n";
	 exit();
       }
       $dist = substr($string, $token_start,$len);
       $t = sprintf("%g",$dist);
       # Finish the child node and put it into parent node
       if (! defined $subtree) {
         print "missing colon ':' prior to here\n", substr($string,0,$i+1), "\n";
         exit();
       }
       $subtree->set_t($t);		# finish the child node
       push @clist, $subtree;		# add child to this node
       undef $subtree;			# to indicate that no current subtree
       $need_comma = 0;
       $token_start = $i + 1;
       last if ($c eq ')');		# all done if ')' found
     } elsif($c eq '(') {
       # new node, call the function recursively
       ($subtree, $length) = parse_newick($string, $i, $n+1);
       $i = $i+$length;
       $token_start = $i + 1;
     } # c eq

  } #for i
  if($c ne ')') {
    print "missing right parentheses ')' here:\n", substr($string,0,$i+1), "\n";
    exit();
  }
  # Root node will have name "_$n_"
  my $obj = Tree->new('-name' => "_$n\_", '-clist' => \@clist);
  return ($obj, $i-$index+1);
} # parse_newick

=head2
Title   : is_star
Function: checks if the given tree is a star topology
Returns : true if tree is a star tree
Args    : None.
=cut
sub is_star {
  $self = shift;     # calling object
  my $is_star = 1;
  foreach $c (@{$self->get_children}) {
    if ($c->is_leaf == 0) {  # all the children of a star tree
                             # should be leaves
      $is_star = 0;          # not a star tree
    }
  } # for c
  return($is_star);
} # is_star

=head2
Title   : is_leaf
Function: checks if the given node is a leaf
          a leaf does not have clist
Returns : true if node is a leaf else returns false
Args    : None.
=cut
sub is_leaf {
  $self = shift;
  return(!defined $self->{clist});
} # is_leaf


=head2
Title   : get_branch_len
Function: get the branch length, of a particular node, from its parent
Returns : a float
Args    : node name
=cut

sub get_branch_len{
  my(
  $self,
  $node_name
  ) = @_;
  foreach $c (@{$self->get_children}) {
    if($c->name eq $node_name) {
      return $c->get_t;
    }# if
  } # foreach
} # get_br_len


=head2
Title   : get_children
Function: get the list of children of the given node
Returns : ref. to an array of strings or empty list if no children
Args    : None.
=cut
sub get_children {
  $self = shift;
  if(!defined $self->{clist}){
    return([]);
  } else {
    return $self->{clist};
  }
} # get_children

sub get_name{
  $self = shift;
  return $self->{name};
}
sub get_t{
  $self = shift;
  return $self->{t};
}
sub set_t{
  my ($self, $t) = @_;
  return $self->{t} = $t;
}
sub get_mu0{
  $self = shift;
  return $self->{mu0};
}
sub set_mu0{
  my ($self, $mu0) = @_;
  return $self->{mu0} = $mu0;
}
sub get_mu1{
  $self = shift;
  return $self->{mu1};
}
sub set_mu1{
  my ($self, $mu1) = @_;
  return $self->{mu1} = $mu1;
}
sub get_row{
  $self = shift;
  return $self->{row};
}
sub set_row{
  my ($self, $row) = @_;
  return $self->{row} = $row;
}

=head2
Title   : set_all_mu
Function: calculate value of mu0 and mu1 for the
          background and motif, respectively, for each
          branch of the tree and store thme in the tree.
          mu = 1 - exp(-lambda * t);
Returns :
Args    :
=cut
sub set_all_mu {
  my (
    $self,
    $lambda0,			# mutation rate 0
    $lambda1			# mutation rate 1
  ) = @_;

  foreach my $child (@{$self->get_children()}) {
    my $t = $child->get_t();
    $child->set_mu0(1 - (exp(-$lambda0 * $t)));
    $child->set_mu1(1 - (exp(-$lambda1 * $t)));
    unless ($child->is_leaf()) { $child->set_all_mu($lambda0, $lambda1); }
  } # loop over children
} # set_all_mu

=head2
Title   : set_all_rows
Function: set the row attribute for each leaf node
	  by looking up the sequence name in the dictionary.
Returns :
Args    : id_row_dict, ref. to id => row dictionary
=cut
sub set_all_rows {
  my (
    $self,
    $id_row_dict
  ) = @_;
  foreach my $child (@{$self->get_children()}) {
    if ($child->is_leaf) {
      $child->set_row($id_row_dict->{$child->get_name});
    } else {
      $child->set_all_rows($id_row_dict);
    }
  } # loop over children
} # set_all_rows

#
# print
#
# Print the node and all of its children recursively
sub print {
  my (
    $self,
    $prefix		# string to print before each line
  ) = @_;
  printf("%sname: '%s' t: %.4f mu0: %.5f mu1: %.5f row: %d\n",
    $prefix, $self->get_name(), $self->get_t(), $self->get_mu0, $self->get_mu1,
    $self->get_row);
  $prefix .= "  ";
  foreach my $child (@{$self->get_children()}) {
    $child->print($prefix);
  }
} # print_node


1;
