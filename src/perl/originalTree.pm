=head1 NAME Tree.pm
This package returns a tree object.
Input:
  filename containing tree in Newick format.
  (http://evolution.genetics. washington.edu/phylip/newicktree.html).
  e.g. (B:6.0,(A:5.0,C:3.0,E:4.0):5.0,D:11.0), where
  letters represent the leaves of a tree. The two matched set of parenteses
  represent the internal nodes. Numbers after the colons are the branch length
  from the node. The internal node is represented by the enclosing parentheses.
=cut

#code begins here

package Tree1;


sub new{

  $class = shift;
  %args = @_;
  $self = {};
  bless $self, ref($class) || $class;
  $self->{rooted} = 0;
  if($args{-tree_file}) {
    open (FH,$args{-tree_file}) || die "file $filename not found $!\n";
    $tree_info = <FH>;
    $self->{branch_len} = _parse_tree($tree_info);
###########?????????? temporarily setting the flag to be true
########## decision should be made depending on whether input tree is a star or a rooted one
    $self->{star} = 1;
    close(FH);
  }

  return $self;
}


sub branch_length{
  $self = shift;
  return $self->{branch_len};
  }

sub star{
  $self = shift;
  return $self->{star};
  }

sub rooted{
  $self = shift;
  return $self->{rooted};
  }



=head2
Title   : _parse_tree
Function: parse the tree given in Newick format
          (seq1:branch1,seq2:branch2,seq3:branch3)
Returns : a ref. to a hash table containing tree information
          hash{seq_name} = branch_length
Args    : none.
=cut
sub parse_tree{
  my @array = (); # temp array to strore tree info.
  my %tree = ();  # hash stores tree information
                  # in seq_name => branch_len format.
  $tree_info = shift;
  $tree_info =~ s/\(//g; # remove the parentheses
  $tree_info =~ s/\)//g; # remove the parentheses
  @array = split(/,/,$tree_info);
  foreach my $val (@array) {
    my ($seq_name, $branch_len) = split(/:/,$val);
    $tree{$seq_name} = $branch_len; # store values in a hash table
  }
return \%tree;
}

sub DESTROY  {
    # nothing
}
1;
