=head1 NAME complexScoring.pm

############### Scoring Function 4 #############################

INPUT:

  1) Gapless blocks obtained from Clustal W multiple alignment (MA).
  2) PSPM (position specific probability matrix) 
  3) background model (zero-order Markov model)
     PSPM and background_freq are used to generate PSSM on runtime.
  4) Tree in Newick format is star topology.
     e.g. (A:5.3, B:2.6, C:12.0) where, 
       i) The letters represent the leaves of a tree, here taken as the seq_name 
          given in MA..
       ii) The matched set of parentheses represents the central node. 
       iii) Numbers after colons are the branch length from the node.
  5) Value of lambda and lambda0 for motif model and background, respectively.
     [Note: lambda is the neutral mutation rate.]
  
OUTPUT:
  
  <Seq_ID  Position   strand  Score>+

PSEUDOCODE:
    
    1) Parse the tree to get a hash table t{seq_name} = branch_length (t) 
       from an internal node to the leaf seq_name.  
    2) Calculate mu{seq_name} = 1- exp(-lambda t), the probability of
       mutation over branch length t, for the motif model.
    3) Calculate mu0{seq_name} = 1- exp(-lambda0 t), the probability of
       mutation over branch length t, for the background.
    4) Compute the probability of the column given the model and branch length.
       p_sigma_motif = 1
       For each column 'i' in the alignment of length w:

         p_col = 0			// P(sigma_i | M_i, T)
         For each (ancestral) base 'a' in alphabet:
           p_col_given_a = 1           // P(sigma_i | a, M_i, T)
           For each base 'b' in column i:
             p_b_given_a = mu_i * f_bi // P(b | a, M_i, t)
             if (a == b) p_b_given_a += 1 - mu_i
 #            p_col_given_a *=  p_b_given_a

         p_col += p_col_given_a * f_ai

       p_sigma_motif *= p_col

          Note that value of f_bi, the prob. of seeing b at ith position,
          comes from PSSM and value of w is equal to the no. of col. of PSSM.

    5) Step 4) is repeated to calculated Prob_sigma_back, the
       probability of the alignment given the background and the tree.

    6) COMPUTE THE SCORE:
             Score = log_base_2 (p_sigma_motif / p_sigma_back)
=cut


#code begins here
package complexScoring;


use PSFM;
use MA;
use Substitution;
use Tree;

@alphabet = qw(A C G T);	# DNA sequence alphabet
@comp_alphabet = qw(T G C A);
#%dna_comp = {'A' => 'T', 'C' => 'G', 'G' => 'T', 'T' => 'A'};
$_ncalls = 0;
$LOG2 = log(2.0);

%known_probs = {};		# Dictionary to hold probabilities that
				# we have already computed for reuse.
				# Key is "type"."strand"."motif column index"."alignment column string"

=head2
Title   : scoreMA
Function: search for a pattern/motif and prints out the maximum
          of the match_score on the positive and neg. strand.
Returns : prints out the scores on the std. output.
Args    : %args, everything passed through command line. 
=cut

sub scoreMA {
  my $class = shift;
  my %args = @_;

  # Get the sname
  my $sname = $args{-sname} || 'SNAME';

  # Get matrix obj. for + and - strand
  my $mat_plus = PSFM->get_matrix(%args);
  #$mat_plus->print;
  my $mat_minus = $mat_plus->rev_comp_PSFM;
  #$mat_minus->print;
  my $w = $mat_plus->get_len_motif;

  # get background frequencies
  my $bkg_freq = $mat_plus->get_bg_freqs;	# ref. to freq. vector
  # combine A/T and C/G in background model
  $bkg_freq->[0] = $bkg_freq->[3] = ($bkg_freq->[0] + $bkg_freq->[3])/2.0;
  $bkg_freq->[1] = $bkg_freq->[2] = ($bkg_freq->[1] + $bkg_freq->[2])/2.0;
  # foreach $f (@$bkg_freq) { print "$f\n";} exit();
  my @bkg_matrix = ($bkg_freq) x $w;		# make phony background "motif"

  # get the tree object 
  my $tree;
  if(defined $args{-tree_file}) {
    $tree = Tree->read_newick($args{-tree_file}); 
  } else { 
    print "No tree information was given \n";
    exit();
  }

  # Get values of lambda.
  # neutral mutation rate for the background.
  my $lambda0  = ($args{-lambda0}) || die ("Plz give the value of lambda0");
  # neutral mutation rate for the motif model
  my $lambda1  = ($args{-lambda}) || die ("Plz give the value of lambda");  

  # Compute mu0 and mu1 and store in tree.
  $tree->set_all_mu($lambda0, $lambda1);

  # Get block objects
  my $list_ma_obj = MA->read_gapless_file(%args);  # ref. to list of ma objects
  
  # set the id,row correspondences in the Tree leaves
  my $ma = $list_ma_obj->[0];
  my $id_row_dict = $ma->get_id_row_dict;
  $tree->set_all_rows($id_row_dict);

  #
  # Substitution models:
  #
  my ($motif_subs_model, $bkg_subs_model);
  if (defined $args{-m_sub_model}) {
    $motif_subs_model = get_subst_model($args{-m_sub_model});
  } 
  if (defined $args{-b_sub_model}) {
    $bkg_subs_model = get_subst_model($args{-b_sub_model});
  }

  # Print the header line of the output.
  print STDOUT "# Hits for",$mat_plus->ID,"  AC=",$mat_plus->AC," in ",$mat_plus->species ,"\n\n";

  my $motif_width = $mat_plus->get_len_motif();

  foreach my $ma (@$list_ma_obj) {		# loop over MA objects
    #$ma->print;
    my $ma_width = $ma->get_width;
    my $offset = $ma->get_offsets->[0]; 	# Offset of the topmost seq. of MA

    for (my $wdw=0; $wdw<$ma_width-$motif_width+1; $wdw++) {	# site
      my ($score, $strand) = compute_site_score(
	$ma,					# multiple alignment
	$wdw,					# offset in alignment
	$mat_plus->get_freq_matrix,		# ref. to motif frequency matrix for strand +
	$mat_minus->get_freq_matrix,		# ref. to motif frequency matrix for strand -
	$tree,				# phylogeny tree
	$lambda0,
	$lambda1, 
	\@bkg_matrix,
	$motif_subs_model,			# ref. to substitution model function
	$bkg_subs_model
      );

      my $coord = $wdw + $offset;
      print STDOUT "$sname\t$strand\t$coord\t$score\n";
    } # site
  } # block
} # scoreMA

=head2
Title   : compute_site_score
Function: computes the score for a site given motif
          and the same given the background
Returns : Returns the log-odds score
Args    :
=cut

sub compute_site_score {
  my(
    $ma,           # MA object 
    $wdw_index,    # offset in alignment of site
    $matrix_plus,  # ref. to motif matrix
    $matrix_minus, # ref. to motif matrix
    $tree,         # Tree object 
    $lambda0,      # Value of lambda for the background.
    $lambda1,      # Value of lambda for the motif
    $bkg_matrix,   # ref. to background "motif" matrix
    $motif_subst_model, # ref. to substitution model function
    $bkg_subst_model  # ref. to substitution model function
  ) = @_;

  # get site strand probability on positive strand given motif model
  my $p_site_given_motif_plus = prob_site(
    0,					# plus strand
    $ma,
    $wdw_index,
    $tree,
    $matrix_plus,
    $lambda1,
    1,					# use mu1 from tree
    $motif_subst_model
  );

  # get site probability on negative strand given motif model
  my $p_site_given_motif_minus = prob_site(
    1,					# minus strand
    $ma,
    $wdw_index,
    $tree,
    $matrix_minus,
    $lambda1,
    1,					# use mu1 from tree
    $motif_subst_model
  );

  # get site probability given background model
  my $p_site_given_bkg = prob_site(
    0,	   				# prob same on both strands
    $ma,
    $wdw_index,
    $tree,
    $bkg_matrix,
    $lambda0,
    0,					# use mu0 from tree 
    $bkg_subst_model
  );

  # return the maximum score and strand
  my $s, $p_site_given_motif;
  if ($p_site_given_motif_plus >= $p_site_given_motif_minus) {
    $p_site_given_motif = $p_site_given_motif_plus;
    $s = '+';
  } else {
    $p_site_given_motif = $p_site_given_motif_minus;
    $s = '-';
  }
  my $site_score = log($p_site_given_motif / $p_site_given_bkg) / $LOG2;

  #print STDERR "p1 $p_site_given_motif p2 $p_site_given_bkg score $site_score\n";

  return($site_score, $s);
} # compute_site_score

=head2
Title   : prob_site
Function: Compute the probability of a site given a model
Returns :
Args    :
=cut
sub prob_site {
  my (
    $strand,		# 0 or 1
    $ma, 		# MA object 
    $wdw_index,      	# Index of the sliding window
    $tree,           	# Tree object
    $freq_matrix,    	# ref. to motif matrix
    $lambda,         	# Value of lambda
    $type,           	# 0=use mu0, 1=use mu1
    $prob_b_given_a  	# ref. to substitution model function
  ) = @_;

  my $w = $#$freq_matrix + 1;	# width of freq_matrix
  my $p_site;

  # get probability of site: product of columns
  for (my $i=0, $p_site=1; $i<$w; $i++) {	# column
    my $col = $ma->get_col($wdw_index+$i),	# multiple alignment column
    my $i_motif = ($type == 0) ? 0 : $i;	# all columns same in type 0
    my $key = "$type.$strand.$i_motif.$col";
    my $other_strand = $strand==0 ? 1 : 0;
    my $other_i = ($type == 0) ? 0 : $w - $i_motif - 1;
    my $other_col = $col; $other_col =~ tr/[ACGT]/[TGCA]/;
    my $other_key = "$type.$other_strand.$other_i.$other_col";
    my $p_col = $known_probs{$key};
#print STDERR "key $key p_col $p_col\n";
    if (!defined $p_col) { 
      $p_col = prob_col(
	$col,				# multiple alignment column
	$freq_matrix->[$i],		# motif column
	$tree,				# phylogeny
	$lambda,			# mutation rate
	$type,           		# 0=use mu0, 1=use mu1
	$prob_b_given_a			# ref. to substitution model function
      );
      $_ncalls++;
#print STDERR "$_ncalls getting p_col $p_col key $key other_key $other_key\n";
      $known_probs{$key} = $know_probs{$other_key} = $p_col;	# save it for possible reuse
    }

    $p_site *= $p_col;
  }
  #print STDERR "p_site = $p_site\n";

  return($p_site);
} # prob_site

=head2
Title   : prob_col
Function: calculates probability of the column 
Returns :
Args    :
=cut
sub prob_col {
  my(
    $ma_col,         # MA column (string)
    $f,        	     # ref. to freq. vector
    $tree,           # Tree object
    $lambda,         # neutral mutation rate
    $type,   	     # 0=use mu0, 1=use mu1
    $prob_b_given_a  # ref. to substitution model function
  ) = @_;

  my $p_col = 0;			# value to return

  $i = 0;
  foreach my $a (@alphabet) {
    $prob_subtree_given_a = prob_subtree_given_a(
      $tree,			# subtree
      $a,			# letter at root of subtree
      $ma_col,			# alignment column
      $f,			# ref. to freq. vector
      $lambda,			# mutation rate
      $type,   	     		# 0=use mu0, 1=use mu1
      $prob_b_given_a		# ref. to substitution model function
    );
    my $p = $prob_subtree_given_a * $f->[$i++];
    $p_col += $p;
  } # loop over b

  return($p_col);
} # prob_col

#
# prob_subtree_given_a
#
# Compute the probability of a tree using the pruning algorithm.
#
sub prob_subtree_given_a {
  my(
    $u,            # root node of subtree
    $a,            # Letter at node 'u'
    $alg_col,      # Alignment column (string) 
    $f,            # ref. to freq. vector (equilibrium distribution)
    $lambda,       # Mutation rate
    $type,   	   # 0=use mu0, 1=use mu1
    $prob_b_given_a  # ref. to substitution model function
  ) = @_;

  my $p;			# probability of u

  if ($u->is_leaf) {    	# u is a leaf
    my $i = $u->get_row;		# row of sequence id at leaf u
    my $x_u = substr($alg_col, $i, 1);	# letter at leaf u
    $p = ($x_u eq $a || $x_u eq '*') ? 1 : 0;		# p = I(x_u, a)
    #print STDERR "leaf ", $u->get_name, " x_u $x_u a $a p= $p\n";

  } else {			# u is not a leaf
    my $clist = $u->get_children;	# children of u
    $p = 1;				# probability of u
    foreach my $v (@$clist) {		# for child v of u
      my $p_v = 0;			# probability of v
      my $mu = $type==0 ? $v->get_mu0 : $v->get_mu1;	# precomputed value of mu
      my $i = 0;
      foreach $b (@alphabet) {		# for base in alphabet
        my $t_v = $v->get_t;		# distance from u to v
        my $b_given_a = &$prob_b_given_a(
	  $b,
	  $a,
	  $f->[$i++],
	  $lambda,
	  $mu,
	  $t_v
        );
        #my $b_given_a = ($mu * $f->[$i++]) + (($a eq $b) ? (1 - $mu) : 0); # Fixation model
        my $v_given_b = prob_subtree_given_a(
	  $v,            # root node of subtree
	  $b,            # Letter at node 'u'
	  $alg_col,      # Alignment column
	  $f,            # equilibrium distribution; b=>f_b
	  $lambda,       # Mutation rate
	  $type,   	   # 0=use mu0, 1=use mu1
	  $prob_b_given_a  # ref. to substitution model function
	);

        $p_v += $b_given_a * $v_given_b;
        #printf STDERR "%sb $b a $a b_given_a %.4f v_given_b %.4f p_v %.3e\n", 
        #  $b_given_a, $v_given_b, $p_v;
      } # base
      #printf STDERR "prob of v given $a p_v = %.3e\n", $p_v;
      $p *= $p_v;
      #print STDERR "p $p\n";
    } # child
  } 

  if (0 && ! $u->is_leaf) {
    print STDERR "$alg_col";
    printf STDERR "p_subtree given a $a p_subtree %.3e\n", $p;
    printf "lambda $lambda type $type f: %.3f %.3f %.3f %.3f\n", $f->[0], $f->[1], $f->[2], $f->[3];
    $u->print("");
  }
  return($p);
} # prob_subtree_given_a

sub DESTROY {
    # nothing
}
1;

