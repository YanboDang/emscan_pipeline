=head1 NAME evoScore.pm

package evoScore.pm

=head2
Title   : score_ma
Function: Score each position of a multiple alignment.  The score is
	    MAX(
              log_2(Pr(site on + | motif) / Pr(site | bkg),
              log_2(Pr(site on - | motif) / Pr(site | bkg)
            ).
	Probabilities are computed using the given evolutionary models
	and the phylogeny tree.

	Gaps are represented as "-" and match any character.

Returns : prints out the scores on standard output
=cut
sub score_ma {
  my(
    $rname,             # name of region
    $ma,                # multiple alignment; MA
    $mat_plus,		# positive strand motif; ref. to array of freq. vectors
    $mat_minus,		# negative strand motif; ref. to array of freq. vectors
    $bkg_matrix,	# background motif (both strands); ref. to array of freq. vectors
    $tree,              # phylogeny tree; Tree
    $lambda0,           # background mutation rate; real
    $lambda1,           # motif mutation rate; real
    $motif_subst_model, # motif Pr(a|b,t); ref. to Substitution function
    $bkg_subst_model    # background Pr(a|b,t); ref. to Substitution function
  ) = @_;


  # globals
  $LOG2 = log(2.0);
  %known_probs = ();		# Dictionary to hold probabilities that
				# we have already computed for reuse.
				# Key is "type"."strand"."motif column index"."alignment column string"

  my $w = $#$mat_plus + 1;			# motif width
  my $ma_width = $ma->get_width;		# MA width
  my $offset = $ma->get_offsets->[0];         	# Offset of the topmost seq. of MA
  my $key_offset = $offset;			# site position in key sequence

  for (my $i=0; $i<$ma_width-$w+1; $i++) {	# site
#print STDERR "pos=$i\t";
    my ($score, $strand) = compute_site_score(
      $ma,				# multiple alignment
      $i,				# offset in alignment
      $mat_plus,			# ref. to motif frequency matrix for + strand
      $mat_minus,			# ref. to motif frequency matrix for - strand
      $bkg_matrix,			# background motif (both strands); ref. to array of freq. vectors
      $tree,				# phylogeny tree
      $lambda0,				# background mutation rate; real
      $lambda1, 			# motif mutation rate; real
      $motif_subst_model,		# motif Pr(a|b,t); ref. to Substitution function
      $bkg_subst_model			# background Pr(a|b,t); ref. to Substitution function
    );

    # ignore gaps in sequence 0 (key sequence)
    my $site_offset;
    if (substr($ma->get_col($i), 0, 1) eq '-') {
      $site_offset = 'X';
    } else {
      $site_offset = $key_offset++;
    }

    my $algn_offset = $i + 1;		# alignment offset
    printf STDOUT "$rname\t$strand\t$algn_offset\t$site_offset\t%.3f\n", $score;

  } # site

} # score_ma

=head2
Title   : compute_site_score
Function: computes the score for a site given motif
          and the same given the background
Returns : Returns the log-odds score
Args    :
=cut

sub compute_site_score {
  my(
    $ma,           # MA object
    $wdw_index,    # offset in alignment of site
    $matrix_plus,  # ref. to motif matrix
    $matrix_minus, # ref. to motif matrix
    $bkg_matrix,   # ref. to background "motif" matrix
    $tree,         # Tree object
    $lambda0,      # Value of lambda for the background.
    $lambda1,      # Value of lambda for the motif
    $motif_subst_model, # ref. to substitution model function
    $bkg_subst_model  # ref. to substitution model function
  ) = @_;

  # get site probability given background model
  my $p_site_given_bkg = prob_site(
    0,	   				# prob same on both strands
    $ma,
    $wdw_index,
    $tree,
    $bkg_matrix,
    $lambda0,
    0,					# use mu0 from tree
    $bkg_subst_model
  );

  # get site strand probability on positive strand given motif model
  my $p_site_given_motif_plus = prob_site(
    0,					# plus strand
    $ma,
    $wdw_index,
    $tree,
    $matrix_plus,
    $lambda1,
    1,					# use mu1 from tree
    $motif_subst_model
  );

  # get site probability on negative strand given motif model
  my $p_site_given_motif_minus = prob_site(
    1,					# minus strand
    $ma,
    $wdw_index,
    $tree,
    $matrix_minus,
    $lambda1,
    1,					# use mu1 from tree
    $motif_subst_model
  );

  #print STDERR "score+ =$p_site_given_motif_plus, score- =$p_site_given_motif_minus\t";
  # return the maximum score and strand
  my $s, $p_site_given_motif;
  if ($p_site_given_motif_plus >= $p_site_given_motif_minus) {
    $p_site_given_motif = $p_site_given_motif_plus;
    $s = '+';
  } else {
    $p_site_given_motif = $p_site_given_motif_minus;
    $s = '-';
  }
  #print STDERR "check::$p_site_given_motif,$p_site_given_bkg\n";
#if($p_site_given_bkg) {
  my $site_score = log($p_site_given_motif / $p_site_given_bkg) / $LOG2;
#}

  #print STDERR "p1 $p_site_given_motif p2 $p_site_given_bkg score $site_score\n";

  return($site_score, $s);
} # compute_site_score

=head2
Title   : prob_site
Function: Compute the probability of a site given a model
Returns :
Args    :
=cut
sub prob_site {
  my (
    $strand,		# 0 or 1
    $ma, 		# MA object
    $wdw_index,      	# Index of the sliding window
    $tree,           	# Tree object
    $motif_matrix,    	# ref. to motif matrix
    $lambda,         	# Value of lambda
    $type,           	# 0=use mu0, 1=use mu1
    $prob_b_given_a  	# ref. to substitution model function
  ) = @_;
  my $p_site;					# return value

  my $w = $#$motif_matrix + 1;			# width of motif
  my $alphabet = $ma->get_alphabet;		# sequence alphabet

  # get probability of site: product of columns
  for (my $i=0, $p_site=1; $i<$w; $i++) {	# column in motif
    my $col = $ma->get_col($wdw_index+$i),	# column in MA
    my $i_motif = ($type == 0) ? 0 : $i;	# all columns same in type 0
    my $key = "$type.$strand.$i_motif.$col";
    my $p_col = $known_probs{$key};
    if (!defined $p_col) {
      $p_col = prob_col(
        $alphabet,			# sequence alphabet; ref to array of strings
	$col,				# multiple alignment column
	$motif_matrix->[$i],		# motif column
	$tree,				# phylogeny
	$lambda,			# mutation rate
	$type,           		# 0=use mu0, 1=use mu1
	$prob_b_given_a			# ref. to substitution model function
      );
      $known_probs{$key} = $p_col;	# save it for possible reuse
      my $other_strand = $strand==0 ? 1 : 0;
      my $other_i = ($type == 0) ? 0 : $w - $i_motif - 1;
      my $other_col = $col; $other_col =~ tr/[ACGT]/[TGCA]/;
      my $other_key = "$type.$other_strand.$other_i.$other_col";
      $known_probs{$other_key} = $p_col;	# save it for possible reuse
    }
    if ($type == 0) {
      $back_prob[$i] = $p_col;
      #printf STDERR "back: %3d %s (%8.6f)\n", $i, $col, $p_col;
    } else {
      #printf STDERR "%3d %8.2f %s (%8.6f/%8.6f)\n", $i, log($p_col/$back_prob[$i])/log(2), $col, $p_col, $back_prob[$i];
    }
    $p_site *= $p_col;
  }

  return($p_site);
} # prob_site

=head2
Title   : prob_col
Function: calculates probability of the column
Returns :
Args    :
=cut
sub prob_col {
  my(
    $alphabet,	     # alphabet; ref. to array of strings
    $ma_col,         # MA column; string
    $f,        	     # motif column; ref. to freq. vector
    $tree,           # Tree object
    $lambda,         # neutral mutation rate
    $type,   	     # 0=use mu0, 1=use mu1
    $prob_b_given_a  # ref. to substitution model function
  ) = @_;

  my $p_col = 0;			# value to return

  $i = 0;
  foreach my $a (@$alphabet) {
    $prob_subtree_given_a = prob_subtree_given_a(
      $alphabet,		# sequence alphabet
      $tree,			# subtree
      $a,			# letter at root of subtree
      $ma_col,			# alignment column
      $f,			# ref. to freq. vector
      $lambda,			# mutation rate
      $type,   	     		# 0=use mu0, 1=use mu1
      $prob_b_given_a		# ref. to substitution model function
    );
    my $p = $prob_subtree_given_a * $f->[$i++];
    $p_col += $p;
  } # loop over b

#print STDERR "pcol=$p_col\t";
  return($p_col);
} # prob_col

#
# prob_subtree_given_a
#
# Compute the probability of a tree using the pruning algorithm.
#
sub prob_subtree_given_a {
  my(
    $alphabet,	   # sequence alphabet
    $u,            # root node of subtree
    $a,            # Letter at node 'u'
    $alg_col,      # Alignment column (string)
    $f,            # ref. to freq. vector (equilibrium distribution)
    $lambda,       # Mutation rate
    $type,   	   # 0=use mu0, 1=use mu1
    $prob_b_given_a  # ref. to substitution model function
  ) = @_;
  my $p;			# probability of u

  if ($u->is_leaf) {    	# u is a leaf
    my $i = $u->get_row;		# row of sequence id at leaf u
    my $x_u = substr($alg_col, $i, 1);	# letter at leaf u
#print STDERR "at i=$x_u\n";
    $p = ($x_u eq $a || $x_u eq '-' || $x_u eq 'N') ? 1 : 0;	# p = I(x_u, a)

  } else {			# u is not a leaf
    my $clist = $u->get_children;	# children of u
    $p = 1;				# probability of u
    foreach my $v (@$clist) {		# for child v of u
      my $p_v = 0;			# probability of v
      my $mu = $type==0 ? $v->get_mu0 : $v->get_mu1;	# precomputed value of mu
      my $i = 0;
      foreach $b (@$alphabet) {		# for base in alphabet
        my $t_v = $v->get_t;		# distance from u to v
        my $b_given_a = &$prob_b_given_a(
	  $b,
	  $a,
	  $f->[$i++],
	  $lambda,
	  $mu,
	  $t_v
        );
        my $v_given_b = prob_subtree_given_a(
          $alphabet,     # sequence alphabet
	  $v,            # root node of subtree
	  $b,            # Letter at node 'u'
	  $alg_col,      # Alignment column
	  $f,            # equilibrium distribution; b=>f_b
	  $lambda,       # Mutation rate
	  $type,   	   # 0=use mu0, 1=use mu1
	  $prob_b_given_a  # ref. to substitution model function
	);

        $p_v += $b_given_a * $v_given_b;
      } # base
      $p *= $p_v;
    } # child
  }

  return($p);
} # prob_subtree_given_a

sub DESTROY {
    # nothing
}
1;
